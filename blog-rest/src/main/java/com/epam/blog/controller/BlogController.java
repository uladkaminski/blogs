package com.epam.blog.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.epam.blog.entity.Blog;
import com.epam.blog.exception.ServiceException;
import com.epam.blog.service.BlogService;

@RestController
@RequestMapping("/blogs")
public class BlogController {

    @Autowired
    private BlogService blogService;

    @ModelAttribute
    private Blog getBlog() {
        return new Blog();
    }

	@RequestMapping(value = "/{blogId}", method = RequestMethod.GET)
	public @ResponseBody Blog readBlog(@PathVariable(value="blogId") Long blogId, HttpServletResponse httpServletResponse) throws ServiceException{
		Blog blog = blogService.read(blogId);
		if(blog != null){
			httpServletResponse.setStatus(HttpServletResponse.SC_CREATED);
			return blog;
		}else{
			httpServletResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
	}
	
	@RequestMapping(method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody List<Blog> readAllBlog() throws ServiceException {
		return blogService.getAllBlogs();
	}
	
	@RequestMapping(value = "/{blogId}", method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody String deleteBlog(@PathVariable(value="blogId") Long blogId) throws ServiceException {
		blogService.delete(blogId);
		return "deleted";
	}
	
	@RequestMapping(value = "/{blogId}", method = RequestMethod.PUT)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody String updateBlog(@PathVariable(value="blogId") Long blogId, Blog blog) throws ServiceException {
		blog.setBlogId(blogId);
		blogService.update(blog);
		return "updated";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.CREATED)
	public Long createBlog(@RequestBody Blog blog) throws ServiceException {
		return blogService.create(blog);
	}
	
	
	
}
