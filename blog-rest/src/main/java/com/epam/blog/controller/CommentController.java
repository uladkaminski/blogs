package com.epam.blog.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.epam.blog.entity.Blog;
import com.epam.blog.entity.Comment;
import com.epam.blog.exception.ServiceException;
import com.epam.blog.service.CommentService;

@RestController
@RequestMapping(value = "/blogs/{blogId}/comments")
public class CommentController {

    @Autowired
    private CommentService commentService;
    
    @RequestMapping(value = "/{commentId}", method = RequestMethod.GET)
    public @ResponseBody Comment readComment(@PathVariable(value = "commentId") Long commentId,
            @PathVariable(value = "blogId") Long blogId, HttpServletResponse httpServletResponse)
                    throws ServiceException {
        Comment comment = commentService.read(commentId);
        if (comment != null && comment.getBlog().getBlogId().equals(blogId)) {
            httpServletResponse.setStatus(HttpServletResponse.SC_OK);
            return comment;
        } else {
            httpServletResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
    }

    @RequestMapping(value = "/{commentId}", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public @ResponseBody String updateComment(@PathVariable(value = "commentId") Long commentId, Comment comment)
            throws ServiceException {
        comment.setCommentId(commentId);
        commentService.update(comment);
        return "updated";
    }

    @RequestMapping(value = "/{commentId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public @ResponseBody String deleteComment(@PathVariable(value = "commentId") Long commentId)
            throws ServiceException {
        commentService.delete(commentId);
        return "updated";
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    public @ResponseBody Long createComment(@RequestBody Comment comment, @PathVariable("blogId") Long blogId)
            throws ServiceException {
        comment.setBlog(new Blog());
        comment.getBlog().setBlogId(blogId);
        return commentService.create(comment);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public @ResponseBody List<Comment> readAllCommentWithBlogId(@PathVariable(value = "blogId") Long blogId)
            throws ServiceException {
        return commentService.readAllCommentWithBlogId(blogId);
    }

}
