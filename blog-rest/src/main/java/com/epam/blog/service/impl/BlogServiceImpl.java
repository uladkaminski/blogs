/**
 * 
 */
package com.epam.blog.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import com.epam.blog.dao.BlogDAO;
import com.epam.blog.entity.Blog;
import com.epam.blog.exception.DAOException;
import com.epam.blog.exception.ServiceException;
import com.epam.blog.service.BlogService;

/**
 * @author Uladzislau Kaminski
 *
 */
public class BlogServiceImpl implements BlogService {
	private static final Logger LOG = Logger.getLogger(BlogServiceImpl.class);

	private BlogDAO blogDAO;

	
	/**
	 * @param blogDAO
	 *            the blogDAO to set
	 */
	public void setBlogDAO(BlogDAO blogDAO) {
		this.blogDAO = blogDAO;
	}

	/**
	 * @see com.epam.blog.service.CommonService#create(java.lang.Object)
	 */
	@Override
	public Long create(Blog entity) throws ServiceException {
		try {
			return blogDAO.create(entity);
		} catch (DAOException e) {
			LOG.error(" Exception during creating Blog ", e);
			throw new ServiceException(" Exception during creating Blog ", e);
		}
	}

	/**
	 * @see com.epam.blog.service.CommonService#read(java.lang.Long)
	 */
	@Override
	public Blog read(Long id) throws ServiceException {
		try {
			return blogDAO.read(id);
		} catch (DAOException e) {
			LOG.error(" Exception during reading Blog ", e);
			throw new ServiceException(" Exception during reading Blog ", e);
		}
	}

	/**
	 * @see com.epam.blog.service.CommonService#update(java.lang.Object)
	 */
	@Override
	public void update(Blog entity) throws ServiceException {
		try {
			blogDAO.update(entity);
		} catch (DAOException e) {
			LOG.error(" Exception during updating Blog ", e);
			throw new ServiceException(" Exception during updating Blog ", e);
		}
	}

	/**
	 * @see com.epam.blog.service.CommonService#delete(java.lang.Object)
	 */
	@Override
	public void delete(Blog entity) throws ServiceException {
		delete(entity.getBlogId());
	}

	/**
	 * @see com.epam.blog.service.CommonService#delete(java.lang.Long)
	 */
	@Override
	public void delete(Long id) throws ServiceException {
		try {
			blogDAO.delete(id);
		} catch (DAOException e) {
			LOG.error(" Exception during deleting Blog ", e);
			throw new ServiceException(" Exception during deleting Blog ", e);
		}

	}

	@Override
	public List<Blog> getAllBlogs() throws ServiceException {
		try {
			return blogDAO.getAllBlogs();
		} catch (DAOException e) {
			LOG.error(" Exception during getting all Blogs ", e);
			throw new ServiceException(" Exception during getting all Blogs ", e);
		}
	}

}
