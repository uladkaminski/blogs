package com.epam.blog.service;

import java.util.List;

import com.epam.blog.entity.Blog;
import com.epam.blog.exception.ServiceException;

public interface BlogService extends CommonService<Blog> {
	List<Blog> getAllBlogs() throws ServiceException;
}
