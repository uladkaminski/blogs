/**
 * 
 */
package com.epam.blog.service;

import java.util.List;

import com.epam.blog.entity.Comment;
import com.epam.blog.exception.ServiceException;

/**
 * @author Uladzislau Kaminski
 *
 */
public interface CommentService extends CommonService<Comment> {
	List<Comment> readAllCommentWithBlogId(Long id) throws ServiceException;
}
