/**
 * 
 */
package com.epam.blog.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.blog.dao.CommentDAO;
import com.epam.blog.entity.Comment;
import com.epam.blog.exception.DAOException;
import com.epam.blog.exception.ServiceException;
import com.epam.blog.service.CommentService;

/**
 * @author Uladzislau Kaminski
 *
 */
public class CommentServiceImpl implements CommentService {
	private static final Logger LOG = Logger.getLogger(CommentServiceImpl.class);

	private CommentDAO commentDAO;
	

	/**
	 * @param commentDAO the commentDAO to set
	 */
	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	/**
	 * @see com.epam.blog.service.CommonService#create(java.lang.Object)
	 */
	@Override
	public Long create(Comment entity) throws ServiceException {
		try {
			return commentDAO.create(entity);
		} catch (DAOException e) {
			LOG.error(" Exception during creating Comment ", e);
			throw new ServiceException(" Exception during creating Comment ", e);
		}
	}

	/**
	 * @see com.epam.blog.service.CommonService#read(java.lang.Long)
	 */
	@Override
	public Comment read(Long id) throws ServiceException {
		try {
			return commentDAO.read(id);
		} catch (DAOException e) {
			LOG.error(" Exception during reading Comment ", e);
			throw new ServiceException(" Exception during reading Comment ", e);
		}
	}

	/**
	 * @see com.epam.blog.service.CommonService#update(java.lang.Object)
	 */
	@Override
	public void update(Comment entity) throws ServiceException {
		try {
			commentDAO.update(entity);
		} catch (DAOException e) {
			LOG.error(" Exception during updating Comment ", e);
			throw new ServiceException(" Exception during updating Comment ", e);
		}
	}

	/**
	 * @see com.epam.blog.service.CommonService#delete(java.lang.Object)
	 */
	@Override
	public void delete(Comment entity) throws ServiceException {
		delete(entity.getCommentId());
	}

	/**
	 * @see com.epam.blog.service.CommonService#delete(java.lang.Long)
	 */
	@Override
	public void delete(Long id) throws ServiceException {
		try {
			commentDAO.delete(id);
		} catch (DAOException e) {
			LOG.error(" Exception during deleting Comment ", e);
			throw new ServiceException(" Exception during deleting Comment ", e);
		}
	}

	@Override
	public List<Comment> readAllCommentWithBlogId(Long id) throws ServiceException {
		try {
			return commentDAO.readAllCommentWithBlogId(id);
		} catch (DAOException e) {
			LOG.error(" Exception during getting all Comments with blogId= " + id , e);
			throw new ServiceException(" Exception during getting all Comments with blogId= " + id, e);
		}
	}

}
