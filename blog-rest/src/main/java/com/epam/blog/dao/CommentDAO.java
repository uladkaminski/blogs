package com.epam.blog.dao;

import java.util.List;

import com.epam.blog.entity.Comment;
import com.epam.blog.exception.DAOException;

public interface CommentDAO extends CommonDAO<Comment> {
	List<Comment> readAllCommentWithBlogId(Long id) throws DAOException;
}	
