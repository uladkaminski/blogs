package com.epam.blog.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.epam.blog.dao.CommentDAO;
import com.epam.blog.entity.Blog;
import com.epam.blog.entity.Comment;
import com.epam.blog.exception.DAOException;

@Transactional(rollbackFor = Exception.class)
public class CommentDAOImpl implements CommentDAO {
	private final static Logger LOG = Logger.getLogger(CommentDAOImpl.class);

	private SessionFactory sessionFactory;
	
	/**
	 * @param sessionFactory the sessionFactory to set
	 */
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	public CommentDAOImpl() {
	}

	/**
	 * @see com.epam.blog.dao.CommonDAO#create(java.lang.Object)
	 */
	@Override
	public Long create(Comment entity) throws DAOException {
		LOG.debug("Creating Comment");
		Session session;
		try {
			session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
			throw new DAOException(e);
		}
		Blog blog = (Blog) session.load(Blog.class, entity.getBlog().getBlogId());
		entity.setBlog(blog);
		blog.getCommentList().add(entity);
		session.save(entity);
		return entity.getCommentId();	
	}

	/**
	 * @see com.epam.blog.dao.CommonDAO#read(java.lang.Long)
	 */
	@Override
	public Comment read(Long id) throws DAOException {
		LOG.debug("Reading Comment");
		try {
			Session session = sessionFactory.getCurrentSession();
			return (Comment) session.get(Comment.class, id);
		} catch (HibernateException e) {
			throw new DAOException(e);
		}
	}

	/**
	 * @see com.epam.blog.dao.CommonDAO#update(java.lang.Object)
	 */
	@Override
	public void update(Comment entity) throws DAOException {
		LOG.debug("Updating comment");

		try {
			sessionFactory.getCurrentSession().update(entity);
		} catch (HibernateException e) {
			throw new DAOException(e);
		}
	}

	/**
	 * @see com.epam.blog.dao.CommonDAO#delete(java.lang.Object)
	 */
	@Override
	public void delete(Comment entity) throws DAOException {
		LOG.debug("Deleting Comment");
		try {
			Session session = sessionFactory.getCurrentSession();
			session.delete(entity);
		} catch (HibernateException e) {
			throw new DAOException(e);
		}
	}

	/**
	 * @see com.epam.blog.dao.CommonDAO#delete(java.lang.Long)
	 */
	@Override
	public void delete(Long id) throws DAOException {
		Session session;
		try {
			session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
			throw new DAOException(e);
		}
		Comment comment = (Comment) session.get(Comment.class, id);
		session.delete(comment);

	}
	@Override
	public List<Comment> readAllCommentWithBlogId(Long blogId) throws DAOException {
		Session session;
		try {
			session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
			throw new DAOException(e);
		}
		Blog blog = (Blog) session.get(Blog.class, blogId);
		return blog.getCommentList();
	}

}
