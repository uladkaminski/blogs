/**
 * 
 */
package com.epam.blog.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.blog.exception.DAOException;

/**
 * @author Uladzislau_Kaminski Interface to realize C.R.U.D. operations
 */
public interface CommonDAO<T> {
	/**
	 * Create entity in database
	 * 
	 * @param entity
	 *            entity that should be created
	 * @return id of entity that is created in database
	 * @throws DAOException
	 *             if trouble with connection with database
	 */
	Long create(T entity) throws DAOException;

	/**
	 * Read entity from database
	 * 
	 * @param id
	 *            of entity that should be read
	 * @return Entity from database by id;
	 * @throws DAOException
	 *             if trouble with connection with database
	 */
	T read(Long id) throws DAOException;

	/**
	 * Update entity in database
	 * 
	 * @param entity
	 *            Entity that should be updated
	 * @throws DAOException
	 *             if trouble with connection with database
	 */
	void update(T entity) throws DAOException;

	/**
	 * Delete entity from database
	 * 
	 * @param entity
	 *            Entity that should be deleted
	 * @throws DAOException
	 *             if trouble with connection with database
	 */
	void delete(T entity) throws DAOException;

	/**
	 * Delete entity from database
	 * 
	 * @param id
	 *            of Entity that should be deleted
	 * @throws DAOException
	 *             if trouble with connection with database
	 */
	void delete(Long id) throws DAOException;

	/**
	 * Default method that close connection
	 * 
	 * @param dataSource
	 *            implements DataSource
	 * @param connection
	 *            Connection
	 */

}
