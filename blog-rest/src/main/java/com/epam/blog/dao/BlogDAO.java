package com.epam.blog.dao;

import java.util.List;

import com.epam.blog.entity.Blog;
import com.epam.blog.exception.DAOException;


public interface BlogDAO extends CommonDAO<Blog> {
	
	
	/**
	 * @return All Blogs from DB
	 * @throws DAOException
	 */
	List<Blog> getAllBlogs() throws DAOException;
}
