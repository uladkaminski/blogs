package com.epam.blog.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.epam.blog.dao.BlogDAO;
import com.epam.blog.entity.Blog;
import com.epam.blog.exception.DAOException;

@Transactional(rollbackFor = Exception.class)
public class BlogDAOImpl implements BlogDAO {
	private final static Logger LOG = Logger.getLogger(BlogDAOImpl.class);

	private SessionFactory sessionFactory;

	/**
	 * @param sessionFactory
	 *            the sessionFactory to set
	 */
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * @see com.epam.blog.dao.CommonDAO#create(java.lang.Object)
	 */
	@Override
	public Long create(Blog entity) throws DAOException {
		LOG.debug("Creating Blog");
		try {
			return (Long) sessionFactory.getCurrentSession().save(entity);
		} catch (HibernateException e) {
			throw new DAOException(e);
		}
	}

	/**
	 * @see com.epam.blog.dao.CommonDAO#read(java.lang.Long)
	 */
	@Override
	public Blog read(Long id) throws DAOException {
		LOG.debug("Reading Blog");
		try {
			return (Blog) sessionFactory.getCurrentSession().get(Blog.class, id);
		} catch (HibernateException e) {
			throw new DAOException(e);
		}
	}

	/**
	 * @see com.epam.blog.dao.CommonDAO#update(java.lang.Object)
	 */
	@Override
	public void update(Blog entity) throws DAOException {
		LOG.debug("Updating Blog");

		try {
			sessionFactory.getCurrentSession().update(entity);
		} catch (HibernateException e) {
			throw new DAOException(e);
		}
	}

	/**
	 * @see com.epam.blog.dao.CommonDAO#delete(java.lang.Object)
	 */
	@Override
	public void delete(Blog entity) throws DAOException {
		delete(entity.getBlogId());
	}

	/**
	 * @see com.epam.blog.dao.CommonDAO#delete(java.lang.Long)
	 */
	@Override
	public void delete(Long id) throws DAOException {
		LOG.debug("Deleting Blog");
		try {
			Blog blog = (Blog) sessionFactory.getCurrentSession().load(Blog.class, id);
			sessionFactory.getCurrentSession().delete(blog);
		} catch (HibernateException e) {
			throw new DAOException(e);
		}

	}

	@Override
	public List<Blog> getAllBlogs() throws DAOException {
		LOG.debug("Getting all Blog");
		try {
		Session session = sessionFactory.getCurrentSession();
		return session.createCriteria(Blog.class).list();
		} catch (HibernateException e) {
			throw new DAOException(e);
		}
	}

}
