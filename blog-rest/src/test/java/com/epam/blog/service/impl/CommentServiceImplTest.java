package com.epam.blog.service.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.blog.dao.CommentDAO;
import com.epam.blog.entity.Comment;
import com.epam.blog.exception.DAOException;
import com.epam.blog.exception.ServiceException;
import com.epam.blog.service.CommentService;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ContextConfiguration(locations = {"/testContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class CommentServiceImplTest {

	@Mock
	private CommentDAO mockCommentDAO;
	
	@InjectMocks
	@Autowired
	private CommentService commentService;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
        assertNotNull(commentService);
	}

	@Test
	public void testCreate() throws DAOException, ServiceException {
		Long expected = 1L;
		Comment comment = mock(Comment.class);
		when(mockCommentDAO.create(comment)).thenReturn(1L);
		Long actual = commentService.create(comment);
		verify(mockCommentDAO,times(1)).create(comment);
		assertEquals(expected, actual);
		}	

	@Test
	public void testRead() throws DAOException, ServiceException {
		Comment comment = new Comment();
		when(commentService.read(anyLong())).thenReturn(comment);
		Comment actual = commentService.read(anyLong());
		verify(mockCommentDAO,times(1)).read(anyLong());
		assertEquals(comment, actual);
	}

	@Test
	public void testUpdate() throws ServiceException, DAOException {
		Comment comment = new Comment();
		commentService.update(comment);
		verify(mockCommentDAO,times(1)).update(comment);
		}

	@Test
	public void testDeleteComment() throws ServiceException, DAOException {
		Comment comment = new Comment();
		commentService.delete(comment);
		verify(mockCommentDAO,times(1)).delete(comment.getCommentId());
	}

	@Test
	public void testDeleteLong() throws ServiceException, DAOException {
		commentService.delete(1L);
		verify(mockCommentDAO,times(1)).delete(1L);
	}

}
