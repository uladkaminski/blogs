package com.epam.blog.service.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.epam.blog.dao.BlogDAO;
import com.epam.blog.entity.Blog;
import com.epam.blog.exception.DAOException;
import com.epam.blog.exception.ServiceException;
import com.epam.blog.service.BlogService;

@ContextConfiguration(locations = { "/testContext.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class BlogServiceImplTest {

    @Mock
    private BlogDAO mockBlogDAO;

    @InjectMocks
    @Autowired
    private BlogService blogService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        assertNotNull(blogService);
    }

    @Test
    public void testCreate() throws ServiceException, DAOException {
        Blog blog = new Blog();
        blogService.create(blog);
        verify(mockBlogDAO, times(1)).create(blog);
    }

    @Test
    public void testRead() throws ServiceException, DAOException {
        Long blogId = 1L;
        blogService.read(blogId);
        verify(mockBlogDAO, times(1)).read(blogId);
    }

    @Test
    public void testUpdate() throws ServiceException, DAOException {
        Blog blog = new Blog();
        blogService.update(blog);
        verify(mockBlogDAO, times(1)).update(blog);
    }

    @Test
    public void testDeleteBlog() throws ServiceException, DAOException {
        Blog blog = new Blog();
        blogService.delete(blog);
        verify(mockBlogDAO, times(1)).delete(blog.getBlogId());
    }

    @Test
    public void testDeleteLong() throws ServiceException, DAOException {
        Blog blog = new Blog();
        blogService.delete(blog.getBlogId());
        verify(mockBlogDAO, times(1)).delete(blog.getBlogId());
    }

}
