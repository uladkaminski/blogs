package com.epam.client.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.StringEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(value = "/blogs")
public class ClientController {
    private static final String PREFIX = "http://localhost:8081/blog-rest";
    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody String getAllBlogs(HttpServletResponse servletResponse) {
        URL obj;
        try {
            obj = new URL(PREFIX + "/blogs");
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept", "application/json");
            try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                return response.toString();
            }
        } catch (IOException e) {
            servletResponse.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            return null;
        }

    }

    @RequestMapping(method = RequestMethod.POST)
    public void createBlog(@RequestBody String blog, HttpServletResponse servletResponse) {
        HttpEntity httpEntity;
        try {
            httpEntity = new StringEntity(blog);
            Request.Post(PREFIX + "/blogs").setHeader("Content-Type", "application/json")
                    .setHeader("Accept", "application/json").body(httpEntity).execute();
        } catch (IOException e) {
            servletResponse.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{id}/comments")
    public void createComment(@RequestBody String comment, @PathVariable("id") Long id,
            HttpServletResponse servletResponse) {
        HttpEntity httpEntity;
        try {
            httpEntity = new StringEntity(comment);
            Request.Post(PREFIX + "/blogs/" + id + "/comments").setHeader("Content-Type", "application/json")
                    .setHeader("Accept", "application/json").body(httpEntity).execute();
        } catch (IOException e) {
            servletResponse.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param id
     * @param servletResponse
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteBlog(@PathVariable("id") Long id, HttpServletResponse servletResponse) {
        try {
            Request.Delete(PREFIX + "/blogs/" + id).execute();
        } catch (IOException e) {
            servletResponse.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
