<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<title>Client</title>
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script src="http://malsup.github.com/jquery.form.js"></script>
</head>
<body>
	<script>
		var prefix = "http://localhost:8082/web-client"
		var delay = 50;
		$(document).ready($(function() {
			blogsGet()
		}));

		var blogsGet = function() {
			$
					.ajax({
						type : 'GET',
						url : prefix + '/blogs',
						dataType : 'json',
						contentType : "application/json;charset=utf-8",
						success : function(result) {
							var respContent = "";
							$
									.each(
											result,
											function(i, blog) {
												respContent += "<table border='5' align='center'>"
														+ "<caption>"
														+ blog.title
														+ "    <button onclick='deleteBlog("
														+ blog.blogId
														+ ")'>x</button>"
														+ "</caption>"
														+ "<tr><td align='center'>"
														+ blog.text
														+ "</td></tr>";
												$
														.each(
																blog.commentList,
																function(j,
																		comment) {
																	respContent += "<tr><td>"
																			+ "COMMENT:"
																			+ comment.text
																			+ "</tr></td>";
																});
												respContent += "</table>";
												respContent += "<div align='center'><input type='text' id="
								+ blog.blogId + "><br>";

												respContent += "<button onclick='postComment("
														+ blog.blogId
														+ ")'>Post comment</button><br></div><br><br>";

											})
							$("#blogList").html(respContent);
						},
					});
		}

		var postComment = function(id) {
			if (document.getElementById(id).value != "") {
				var comment = {
					commentId : "",
					text : document.getElementById(id).value,
					blogId : id
				};
				document.getElementById(id).value = "";
				$.ajax({
					type : "POST",
					url : prefix + '/blogs/' + id + '/comments',
					contentType : "application/json; charset=utf-8",
					data : JSON.stringify(comment),
					success : setTimeout(blogsGet, delay),
				});
			} else {
				alert("Write comment!")
			}

		}

		var deleteBlog = function(id) {
			$.ajax({
				type : "DELETE",
				url : prefix + '/blogs/' + id,
				accept : "application/json; charset=utf-8",
				success : setTimeout(blogsGet, delay),
			});
		}

		var postBlog = function() {
			if (document.getElementById("new.blog.text").value != ""
					&& document.getElementById("new.blog.title").value != "") {
				var blog = {
					blogId : "",
					title : document.getElementById("new.blog.title").value,
					text : document.getElementById("new.blog.text").value,
				};
				document.getElementById("new.blog.text").value = "";
				document.getElementById("new.blog.title").value = "";

				$.ajax({
					type : "POST",
					url : prefix + '/blogs',
					contentType : "application/json; charset=utf-8",
					data : JSON.stringify(blog),
					success : setTimeout(blogsGet, delay),
				});
			} else {
				alert("Input all fields!")
			}

		}
	</script>

	<div id="blogList"></div>


	<div>
		<h3>Title:</h3>
	</div>
	<div>
		<input type="text" id="new.blog.title" size="30">
	</div>
	<div>
		<h3>Text:</h3>
	</div>

	<div>
		<input type="text" id="new.blog.text" size="50">
	</div>
	<br>
	<div>
		<button onclick='postBlog()'>post</button>
	</div>

</body>
</html>
